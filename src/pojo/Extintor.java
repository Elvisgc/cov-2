/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pojo;

/**
 *
 * @author Estudiante
 */
public class Extintor {
    private int id;
    private String marca;
    private float capacidad;
    private LISTA_TIPO_EXTINTOR tipo_extintor;
    private LISTA_UND_MEDIDA unidad_medida;
    private long fecha_registro;
    private int cantidad;
    public static enum LISTA_TIPO_EXTINTOR{CO2,QUIMICO_SECO,AGUA_PRESION};
    public static enum LISTA_UND_MEDIDA{LITROS,LIBRAS,UNIDAD};

    public Extintor() {
    }

    public Extintor(String marca, 
            float capacidad, 
            LISTA_TIPO_EXTINTOR tipo_extintor, 
            LISTA_UND_MEDIDA unidad_medida, 
            long fecha_registro, 
            int cantidad) {        
        this.marca = marca;
        this.capacidad = capacidad;
        this.tipo_extintor = tipo_extintor;
        this.unidad_medida = unidad_medida;
        this.fecha_registro = fecha_registro;
        this.cantidad = cantidad;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public float getCapacidad() {
        return capacidad;
    }

    public void setCapacidad(float capacidad) {
        this.capacidad = capacidad;
    }

    public LISTA_TIPO_EXTINTOR getTipo_extintor() {
        return tipo_extintor;
    }

    public void setTipo_extintor(LISTA_TIPO_EXTINTOR tipo_extintor) {
        this.tipo_extintor = tipo_extintor;
    }

    public LISTA_UND_MEDIDA getUnidad_medida() {
        return unidad_medida;
    }

    public void setUnidad_medida(LISTA_UND_MEDIDA unidad_medida) {
        this.unidad_medida = unidad_medida;
    }

    public long getFecha_registro() {
        return fecha_registro;
    }

    public void setFecha_registro(long fecha_registro) {
        this.fecha_registro = fecha_registro;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }
    
    
}
