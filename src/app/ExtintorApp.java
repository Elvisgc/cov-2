/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app;

import implement.ExtintorDaoImpl;
import java.io.IOException;
import java.nio.file.Files;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import pojo.Extintor;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 *
 * @author Estudiante
 */
public class ExtintorApp {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        ExtintorDaoImpl dao = new ExtintorDaoImpl();
        Date fecha = new Date();
        try {
//            dao.save(
//                    new Extintor("Amerex",20.0f,
//                            Extintor.LISTA_TIPO_EXTINTOR.AGUA_PRESION,
//                            Extintor.LISTA_UND_MEDIDA.LITROS,
//                            Calendar.getInstance().getTime().getTime(),
//                            15)
//            );
//            
            List<Extintor> extintores = dao.findAll();
            System.out.format("%5s %20s %20s %20s %20s %20s %20s\n",
                    "ID","Marca","Capacidad","Tipo_extintor","Und_medida",
                    "Fecha_registro","Cantidad");
//            extintores.stream().forEach((e) -> {
//                print(e);
//            });

               Extintor e = dao.findById(1);
               if(e == null){
                   System.out.println("Extintor no encontrado!!");
                   return;
               }
               e.setCapacidad(15);
               //e.setCantidad(115);

               dao.update(e);
               print(e);
        } catch (IOException ex) {
            Logger.getLogger(ExtintorApp.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    private static void print(Extintor e){
        SimpleDateFormat format = new SimpleDateFormat("d/M/y");
        System.out.format("%5d %20s %20.2f %20s %20s %20s %20d\n",
                e.getId(),e.getMarca(),e.getCapacidad(),
                e.getTipo_extintor().toString(),
                e.getUnidad_medida().toString(),
                format.format(new Date(e.getFecha_registro())),
                e.getCantidad());
    }
    
}
