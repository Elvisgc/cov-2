/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import pojo.Extintor;

/**
 *
 * @author Estudiante
 */
public interface IExtintorDao extends IDao<Extintor>{
    Extintor findById(int id)throws IOException;
    List<Extintor>findByMarca(String marca)throws IOException;
    List<Extintor>findByCapacidad(float capacity)throws IOException;
    List<Extintor>findByFechaRegistro(Date fecha)throws IOException;
}
