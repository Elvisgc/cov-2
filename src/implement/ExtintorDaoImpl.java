/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package implement;

import dao.IExtintorDao;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import pojo.Extintor;

/**
 *
 * @author Estudiante
 */
public class ExtintorDaoImpl implements IExtintorDao {

    private RandomAccessFile raf;
    private File file;
    private String filename;

    public ExtintorDaoImpl() {
    }

    private void openRAF() throws FileNotFoundException, IOException {
        filename = "extintor.dat";
        file = new File(filename);
        if (!file.exists()) {
            raf = new RandomAccessFile(file, "rw");
            raf.seek(0);
            raf.writeInt(0);
            raf.writeInt(0);
        } else {
            raf = new RandomAccessFile(file, "rw");
        }
    }

    private void close() throws IOException {
        if (raf != null) {
            raf.close();
        }
    }

    @Override
    public Extintor findById(int id) throws IOException {
        openRAF();
        raf.seek(0);
        int n = raf.readInt();
        if (n == 0) {
            return null;
        }
        int index = runBinarySearchRecursively(id, 0, n - 1);
        if (index <= -1) {
            return null;
        }
        Extintor extintor = new Extintor();
        long pos = 8 + index*71;
        raf.seek(pos);
        extintor.setId(raf.readInt());
        extintor.setMarca(raf.readUTF().replace('\0', ' ').trim());
        extintor.setCapacidad(raf.readFloat());
        extintor.setTipo_extintor(
                Extintor.LISTA_TIPO_EXTINTOR.values()[raf.readInt()]);
        extintor.setUnidad_medida(
                Extintor.LISTA_UND_MEDIDA.values()[raf.readInt()]);
        extintor.setFecha_registro(raf.readLong());
        extintor.setCantidad(raf.readInt());

        close();
        return extintor;
    }

    public int runBinarySearchRecursively(int key, int low, int high) throws IOException {
        int middle = (low + high) / 2;

        if (high < low) {
            return -1;
        }
        long pos = 8 + 71 * middle;
        raf.seek(pos);
        int id = raf.readInt();
        if (key == id) {
            return middle;
        } else if (key < id) {
            return runBinarySearchRecursively(
                    key, low, middle - 1);
        } else {
            return runBinarySearchRecursively(
                    key, middle + 1, high);
        }
    }

    @Override
    public List<Extintor> findByMarca(String marca) throws IOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Extintor> findByCapacidad(float capacity) throws IOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Extintor> findByFechaRegistro(Date fecha) throws IOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void save(Extintor t) throws IOException {
        openRAF();
        raf.seek(0);
        int n = raf.readInt();
        int k = raf.readInt();

        long pos = 8 + 71 * n;

        raf.seek(pos);

        raf.writeInt(++k);
        raf.writeUTF(limitString(t.getMarca(), 20));
        raf.writeFloat(t.getCapacidad());
        raf.writeInt(t.getTipo_extintor().ordinal());
        raf.writeInt(t.getUnidad_medida().ordinal());
        raf.writeLong(t.getFecha_registro());
        raf.writeInt(t.getCantidad());

        raf.seek(0);
        raf.writeInt(++n);
        raf.writeInt(k);
        close();
    }

    private String limitString(String text, int capacidad) {
        StringBuilder builder;
        if (text == null) {
            builder = new StringBuilder(capacidad);
        } else {
            builder = new StringBuilder(text);
            builder.setLength(capacidad);
        }
        return builder.toString();
    }

    @Override
    public boolean update(Extintor t) throws IOException {
        boolean flag = false;
        openRAF();
        raf.seek(0);
        int n = raf.readInt();
        
        if(n <= 0){
            return flag;
        }
        int index = runBinarySearchRecursively(t.getId(), 0, n-1);
        if(index == -1){
            return flag;
        }
        
        long pos = 12 + index*71;
        raf.seek(pos);
        
        raf.writeUTF(limitString(t.getMarca(), 20));
        raf.writeFloat(t.getCapacidad());
        raf.writeInt(t.getTipo_extintor().ordinal());
        raf.writeInt(t.getUnidad_medida().ordinal());
        raf.writeLong(t.getFecha_registro());
        raf.writeInt(t.getCantidad());
        flag = true;
        close();
        
        return flag;
    }

    @Override
    public boolean delete(Extintor t) throws IOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Extintor> findAll() throws IOException {
        List<Extintor> extintores = new ArrayList<>();
        openRAF();
        raf.seek(0);
        int n = raf.readInt();
        for (int i = 0; i < n; i++) {
            long pos = 8 + 71 * i;
            raf.seek(pos);
            Extintor extintor = new Extintor();

            extintor.setId(raf.readInt());
            extintor.setMarca(raf.readUTF().replace('\0', ' ').trim());
            extintor.setCapacidad(raf.readFloat());
            extintor.setTipo_extintor(
                    Extintor.LISTA_TIPO_EXTINTOR.values()[raf.readInt()]);
            extintor.setUnidad_medida(
                    Extintor.LISTA_UND_MEDIDA.values()[raf.readInt()]);
            extintor.setFecha_registro(raf.readLong());
            extintor.setCantidad(raf.readInt());
            extintores.add(extintor);
        }
        close();
        return extintores;
    }

}
